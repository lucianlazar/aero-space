package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.DrawableGameObject;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core.RectTransform;
import com.ykyriacou.aero_space.model.HealthBarModel;

/**
 * Whoever created/controls the HealthBar is responsible of:
 *  - manually positioning it
 *  - manually calling setFillAmount01(float) when needed
 *  - destroying it
 *
 * Starts full (_FillAmount = 1f)
 */
public class HealthBar extends DrawableGameObject {
    private HealthBarModel _HealthBarModel;
    private float _FillAmount;
    private Paint _Paint;
    private RectTransform _FillDrawRectTransform;

    public HealthBar(HealthBarModel healthBarModel) {
        super(healthBarModel.image);
        _Paint = new Paint();
        _HealthBarModel = healthBarModel;
        _FillDrawRectTransform = new RectTransform(new Rect());
    }

    @Override
    public void onStart() {
        super.onStart();

        rectTransform.setSize(_HealthBarModel.size);
        _FillDrawRectTransform.rect.set(rectTransform.rect);

        setFillAmount01(1f);
    }

    @Override
    public void draw(Canvas canvas) {
        // Drawing it our way
        //super.draw(canvas);

        canvas.drawBitmap(_Image, null, _FillDrawRectTransform.rect, _Paint);
    }

    public void updatePosition(int centerX, int centerY)
    {
        rectTransform.centerTo(centerX, centerY);
        _FillDrawRectTransform.rect.offsetTo(rectTransform.rect.left, rectTransform.rect.top);
    }

    public void setFillAmount01(float fill)
    {
        _FillAmount = fill;
        int paintColor = interpolateColor(_HealthBarModel.colorOnEmpty, _HealthBarModel.colorOnFull, _FillAmount);
        _Paint.setColorFilter(new LightingColorFilter(paintColor, 0));
        int newFillWidth = Math.round(rectTransform.rect.width() * _FillAmount);
        _FillDrawRectTransform.setWidth(newFillWidth);
    }

    private float interpolateFloat(float a, float b, float proportion) {
        return (a + ((b - a) * proportion));
    }

    // Returns an interpolated color, between <a> and <b>
    private int interpolateColor(int a, int b, float proportion) {
        float[] hsva = new float[3];
        float[] hsvb = new float[3];
        Color.colorToHSV(a, hsva);
        Color.colorToHSV(b, hsvb);
        for (int i = 0; i < 3; i++) {
            hsvb[i] = interpolateFloat(hsva[i], hsvb[i], proportion);
        }
        return Color.HSVToColor(hsvb);
    }
}

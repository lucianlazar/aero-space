package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.DrawableGameObject;

/**
 * Simple scrolling background. Uses the same Bitmap twice in order to fill gaps
 */
public class GameBackground extends DrawableGameObject {
    Rect _ScrollingRect;
    int _Velocity;

    public GameBackground(Bitmap imageBitmap, Rect scrollingRect, int velocity)
    {
        super(imageBitmap);
        _ScrollingRect = scrollingRect;
        _Velocity = velocity;
        initRectTransform();
    }

    public void update()
    {
        if (rectTransform.rect.top < _ScrollingRect.bottom) // not went completely below the draw area yet
        {
            rectTransform.rect.offset(0, _Velocity);
        }
        else
        {
            initRectTransform();
        }
    }

    private void initRectTransform() {
        rectTransform.rect.set(_ScrollingRect);
    }

    public void draw(Canvas canvas)
    {
        super.draw(canvas);

        if (rectTransform.rect.top > 0) // background was shifted downwards => fill the space above with a copy of it
        {
            rectTransform.rect.offset(0, -_ScrollingRect.height());
            canvas.drawBitmap(_Image, null, rectTransform.rect, null);
            rectTransform.rect.offset(0, +_ScrollingRect.height());
        }
    }
}

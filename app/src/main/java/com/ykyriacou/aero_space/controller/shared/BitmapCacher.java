package com.ykyriacou.aero_space.controller.shared;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.util.HashMap;

/**
 * Caches bitmaps by resId and their sub-bitmaps (frames, i.e. the animation chunks
 * in the bitmaps containing more than one frame, used as animations)
 *
 * destroy() should be called once the app ends
 */
public class BitmapCacher {
    private static BitmapCacher _Instance;

    private Context _Context;
    private HashMap<Integer, Bitmap> _MapResIdToBitmap;
    private HashMap<Bitmap, Bitmap[]> _MapBitmapToBitmapFrames;

    private BitmapCacher(Context context)
    {
        _Context = context;
        _MapResIdToBitmap = new HashMap<>();
        _MapBitmapToBitmapFrames = new HashMap<>();
    }

    public static void initializeInstance(Context context) {
        _Instance = new BitmapCacher(context);
    }

    public static BitmapCacher getInstance()
    {
        return _Instance;
    }

    public Bitmap getForResId(int resId)
    {
        if (!_MapResIdToBitmap.containsKey(resId))
        {
            _MapResIdToBitmap.put(resId, BitmapFactory.decodeResource(_Context.getResources(), resId));
        }

        return _MapResIdToBitmap.get(resId);
    }

    public Bitmap[] getFrames(Bitmap src, int numFramesHint)
    {
        if (!_MapBitmapToBitmapFrames.containsKey(src))
        {
            Bitmap[] animationFrames = new Bitmap[numFramesHint];

            int width = Math.round((float) src.getWidth() / numFramesHint);
            int height = src.getHeight();

            for (int i = 0; i < numFramesHint; ++i)
            {
                animationFrames[i] = Bitmap.createBitmap(src, i*width, 0, width, height);
            }

            _MapBitmapToBitmapFrames.put(src, animationFrames);
        }

        return _MapBitmapToBitmapFrames.get(src);
    }

    public void destroy()
    {
        _Instance = null;
        for (Bitmap key : _MapBitmapToBitmapFrames.keySet())
        {
            try
            {
                for (Bitmap frame : _MapBitmapToBitmapFrames.get(key))
                    frame.recycle();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        _MapBitmapToBitmapFrames.clear();

        for (int key : _MapResIdToBitmap.keySet())
        {
            try
            {
                _MapResIdToBitmap.get(key).recycle();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        _MapResIdToBitmap.clear();
    }
}

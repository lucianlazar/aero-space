package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects;

import android.graphics.Rect;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core.GameObjectsManager;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core.RectTransform;

import java.util.ArrayList;

/**
 * Base class for any entity in the world that needs a constant update() callback
 *
 * For convenience, any new GameObject is automatically registered by GameObjectsManager (if it's possible),
 * so it's enough to write "new GameObjectOrDerivedClass()" and everything is set up.
 *
 * OBJECTS MUST NOT BE CREATED IN CONSTRUCTORS OF OTHER OBJECTS. THE CONSTRUCTOR IS USED ONLY FOR
 * VERY BASIC INITIALIZATION, LIKE STORING THE PARAMETERS PASSED IN IT.
 * OTHER INITIALIZATION TASKS MUST BE HANDLED IN onStart()
 *
 * In the constructor, the game object requests the game manager to enter the world
 * (i.e. to be registered, receive updates etc.). onDidManagerAcceptObject(bool) is called immediately
 * to let the object know if it will be accepted or not.
 * If not, nothing will happen
 * If yes, then the object's onStart() will soon (in the current or in the next frame) be called,
 * following by update() callbacks in each frame until it's destroyed
 *
 */
public class GameObject {
    public RectTransform rectTransform = new RectTransform(new Rect());
    public String name;

    protected GameObjectsManager _GameObjectsManager;

    private boolean _WasAcceptedByManager, _IsDestroyScheduled, _IsStarted;
    private ArrayList<IOnDestroyListener> _IOnDestroyListeners;

    public GameObject(String name)
    {
        this.name = name;

        _GameObjectsManager = GameObjectsManager.getInstance();

        // Game is about to end => cancel creation
        if (_GameObjectsManager == null)
            return;

        _IOnDestroyListeners = new ArrayList<>();
        onDidManagerAcceptObject(_GameObjectsManager.addGameObject(this));
    }

    protected void onDidManagerAcceptObject(boolean isAcceptedByManager)
    {
        _WasAcceptedByManager = isAcceptedByManager;
    }

    // GameObject entered the world and will receive updates
    public void onStart()
    {
        _IsStarted = true;
    }

    // Called once per frame
    public void update()
    {

    }

    // Called immediately after GameObject.destroy(GameObject) is called
    public void onDestroyScheduled()
    {
        _IsDestroyScheduled = true;
    }

    // Called right before the game object is removed from the world.
    // Will not be called when the game world is being destroyed
    public void onDestroy()
    {
        dispatchOnDestroyEvent();
    }

    public boolean wasAcceptedByManager()
    {
        return _WasAcceptedByManager;
    }

    // True if onStart() was called
    public boolean isStarted()
    {
        return _IsStarted;
    }

    // True if onDestroyScheduled() was called
    public boolean isDestroyScheduled()
    {
        return _IsDestroyScheduled;
    }

    // Possibility to listen for the onDestroy() callback
    public void addOnDestroyListener(IOnDestroyListener onDestroyListener)
    {
        _IOnDestroyListeners.add(onDestroyListener);
    }

    public void removeOnDestroyListener(IOnDestroyListener onDestroyListener)
    {
        _IOnDestroyListeners.add(onDestroyListener);
    }

    public static void destroy(GameObject gameObject)
    {
        gameObject._GameObjectsManager.scheduleGameObjectForRemoval(gameObject);
    }

    private void dispatchOnDestroyEvent()
    {
        for (int i = 0; i <_IOnDestroyListeners.size(); ++i)
        {
            try
            {
                _IOnDestroyListeners.get(i).onGameObjectDestroyed(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        _IOnDestroyListeners.clear();
    }


    public interface IOnDestroyListener
    {
        void onGameObjectDestroyed(GameObject gameObject);
    }
}

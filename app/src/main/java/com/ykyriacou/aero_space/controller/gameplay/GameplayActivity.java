package com.ykyriacou.aero_space.controller.gameplay;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;

import com.ykyriacou.aero_space.controller.shared.GameAudio;
import com.ykyriacou.aero_space.view.gameplay.GameSurfaceView;
import com.ykyriacou.aero_space.R;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors.Spaceship;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core.GameObjectsManager;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.GameplayController;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core.GameThread;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core.GameWorldMetrics;
import com.ykyriacou.aero_space.model.GameplayState;
import com.ykyriacou.aero_space.controller.shared.GameStorage;
import com.ykyriacou.aero_space.controller.shared.BitmapCacher;

/**
 * Starts, initializes, pauses/resumes, destroys the core gameplay components
 *
 *
 */
public class GameplayActivity
        extends Activity
        implements
            GameplayController.IGameplayListener,
            GameSurfaceView.IGameSurfaceViewLifecycleListener
{

    private GameSurfaceView _GameSurfaceView;       // defined in layout
    private GameThread _GameThread;                 // game looper
    private GameWorldMetrics _GameWorldMetrics;     // used for world scaling, defines dimensions of world/actors
    private BitmapCacher _BitmapCacher;             // utility for bitmap re-use
    private UIUpdaterRunnable _UIUpdaterTask;       // handles the callbacks from the gameplay thread on the UI thread
    private GameObjectsManager _GameObjectsManager; // handles game objects lifecycle, registering, destroying etc.
    private boolean _GameEnded;                     // set to true in onGameOver().

    /// Activity callbacks
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gameplay);

        BitmapCacher.initializeInstance(this);
        _BitmapCacher = BitmapCacher.getInstance();

        _GameSurfaceView = (GameSurfaceView) findViewById(R.id.gamePanel);

        _GameObjectsManager = new GameObjectsManager();

        // Tell GameView where to get the drawable objects from
        // in order to send them the draw(Canvas) callback at the end of each frame.
        // Subscribe for its lifecycle, in order to know when the SurfaceView is created, so we
        // can initialize/resume the game mechanics.
        _GameSurfaceView.initialize(_GameObjectsManager.getDrawableGameObjectsArrayRef(), this);

        // Retrieve display size and initialize GameWorldMetrics
        _GameWorldMetrics = GameWorldMetrics.getInstance();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        _GameWorldMetrics.onSizeChanged(displayMetrics.widthPixels, displayMetrics.heightPixels);

        // Create the game's thread, and register for update callbacks to be sent to objects manager
        // and draw callbacks for the SurfaceView.
        // The thread will start, but won't give any callbacks until we call setRunning(true) on it.
        // This is done in resumeMechanics, where we know that the game's SurfaceView is initialized
        _GameThread = new GameThread(_GameObjectsManager, _GameSurfaceView);
        _GameThread.start();

        // Create the game's fate. New GameObjects are automatically added into the game's world by
        // GameObjectsManager. Also, pass this activity as a listener for gameplay
        // callbacks (player died, player score increased etc.)
        new GameplayController(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // _GameEnded will be true if after the player died, he paused the app and re-opened it
        // Game's mechanics should remain paused in this case
        if (_GameEnded)
            return;

        // If the game surface view is not yet initialized, then this means that either the
        // activity was just created or was started from previously being stopped, case in which
        // the mechanics will be resumed in onGameViewCreated()
        if (_GameSurfaceView.isSurfaceViewCreated())
            resumeMechanics();
    }

    @Override
    protected void onPause() {
        pauseMechanics();

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        setGameThreadRunning(false);    // prevent any further update() and draw() callbacks
        _GameObjectsManager.destroy();  // destroy all the game objects immediately and clean the world
        destroyGameThread();            // stop the game thread
        _BitmapCacher.destroy();        // recycle/destroy any cached bitmaps in order to be GC'ed

        super.onDestroy();
    }


    /// GameplayController callbacks
    /// The game thread (on which these callbacks are done) is a separate one
    @Override
    public void onGameplayStateInitialized(GameplayState gameplayState) {
        // Create ui updater the first time the game state is initialized
        _UIUpdaterTask = new UIUpdaterRunnable(gameplayState);
    }

    @Override
    public void onPlayerProjectileDestroyedSpaceship(GameplayState gameplayState, Spaceship spaceship) {
        // Update UI. Must be done on UI thread, not on the game thread, obviously.
        runOnUiThread(_UIUpdaterTask);
    }

    // Pause the game
    // Store score
    // Play game over animation
    @Override
    public void onGameOver(final GameplayState gameplayState) {
        _GameEnded = true;
        pauseMechanics();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                // Play sound effect
                GameAudio.getInstance().playSoundEffect(GameAudio.SFX.GAME_OVER);

                // Store score
                GameStorage.getInstance().addNewScore(gameplayState.spaceshipsDestroyedByPlayersProjectile);

                // Game over animation
                View v = findViewById(R.id.gameOverTextView);
                v.setVisibility(View.VISIBLE);
                v.setAlpha(0f);
                v.animate()
                    .alpha(1f)
                    .setDuration(1000)
                    .setListener(null);


                // Go to main menu after 3 seconds
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try
                        {
                            finish();
                        }
                        catch (Exception e) {e.printStackTrace();}
                    }
                }, 3000);
            }
        });
    }



    /// GameSurfaceView lifecycle callbacks
    @Override
    public void onGameSurfaceViewCreated() {
        // Mechanics are resumed only after the game's SurfaceView was created
        resumeMechanics();
    }

    // We don't need to do anything here, because the onPause is called way sooner than this, so
    // the mechanics pausing is done there.
    @Override
    public void onGameSurfaceViewDestroyed() {}



    /// Private methods
    private void resumeMechanics()
    {
        setGameThreadRunning(true);
    }

    private void pauseMechanics()
    {
        setGameThreadRunning(false);
    }

    private void setGameThreadRunning(boolean running)
    {
        // Sometimes the thread won't stop immediately => try until succeeded
        boolean failed;
        do
        {
            failed = false;

            try
            {
                _GameThread.setRunning(running);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                failed = true;
            }

        }
        while (failed);
    }

    private void destroyGameThread()
    {
        // Sometimes the thread won't stop immediately => try until succeeded
        boolean failedToDestroy;
        do
        {
            failedToDestroy = false;

            try
            {
                _GameThread.kill();
                //_GameThread.join();
            }
            catch (Exception e)
            {
                e.printStackTrace();
                failedToDestroy = true;
            }

        }
        while (failedToDestroy);
    }


    // Runnable that updates the UI from game's state
    // Ran whenever the activity receives relevant state updates
    class UIUpdaterRunnable implements Runnable {
        TextView _ScoreTextView;

        GameplayState _GameplayState;

        public UIUpdaterRunnable(GameplayState gameplayState)
        {
            _GameplayState = gameplayState;
            _ScoreTextView = (TextView) findViewById(R.id.scoreTextView);
        }

        @Override
        public void run() {
            _ScoreTextView.setText(String.valueOf(_GameplayState.spaceshipsDestroyedByPlayersProjectile));
        }
    }
}

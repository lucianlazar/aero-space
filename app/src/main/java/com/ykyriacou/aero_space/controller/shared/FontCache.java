package com.ykyriacou.aero_space.controller.shared;

import android.content.Context;
import android.graphics.Typeface;

import java.util.Hashtable;

/**
 *
 * destroy() should be called once the app ends
 */
public class FontCache {
    private static FontCache _Instance;
    private Hashtable<String, Typeface> fontCache;


    private FontCache()
    {
        fontCache = new Hashtable<>();
    }

    public static FontCache getInstance()
    {
        if (_Instance == null)
            _Instance = new FontCache();

        return _Instance;
    }


    public Typeface get(String name, Context context) {
        Typeface tf = fontCache.get(name);
        if(tf == null) {
            try {
                tf = Typeface.createFromAsset(context.getAssets(), name);
            }
            catch (Exception e) {
                return null;
            }
            fontCache.put(name, tf);
        }
        return tf;
    }

    public void destroy()
    {
        _Instance = null;

        if (fontCache == null)
            return;

        fontCache.clear();
        fontCache = null;
    }
}

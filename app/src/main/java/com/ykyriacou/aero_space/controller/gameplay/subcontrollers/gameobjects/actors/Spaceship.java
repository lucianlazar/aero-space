package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.CollisionDetector;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.GameObject;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.Sprite;
import com.ykyriacou.aero_space.model.SpaceshipModel;

/**
 * Created by NoBody on 07-04-16.
 */
public class Spaceship extends Sprite implements CollisionDetector.ICollider {
    SpaceshipModel _SpaceshipModel;
    HealthBar _HealthBar;
    long _TimeLastShoot;

    // Drawing vars
    float _XScaleFactorFromBitmapToRect, _YScaleFactorFromBitmapToRect;
    float _RotationToSetToDrawMatrix;
    PointF _HalfBitmapSize;
    Matrix _DrawMatrix;
    Rect _ColliderRect;


//    public Spaceship(Bitmap sprite, int numAnimationFrames, int autoShootingDelayMillis, int selfCollisionLayer, int enemiesCollisionLayerMask) {
    public Spaceship(SpaceshipModel spaceshipModel) {
        super(spaceshipModel.spriteModel);
        _SpaceshipModel = spaceshipModel;

        _DrawMatrix = new Matrix();
        _HalfBitmapSize = new PointF();
    }

    @Override
    public void onStart() {
        super.onStart();

        _ColliderRect = new Rect();
        updateColliderRect();

        if (_SpaceshipModel.healthBarModel != null)
            _HealthBar = new HealthBar(_SpaceshipModel.healthBarModel);

        Bitmap frame = _Animation.getCurrentFrameImage(); // all of the animation's frames have the same size
        _XScaleFactorFromBitmapToRect = (float)rectTransform.rect.width()  / frame.getWidth();
        _YScaleFactorFromBitmapToRect = (float)rectTransform.rect.height() / frame.getHeight();
        _HalfBitmapSize.set(frame.getWidth() / 2f, frame.getHeight() / 2f);
        if (_SpaceshipModel.verticalOrientation == SpaceshipModel.VerticalOrientation.DOWN)
            _RotationToSetToDrawMatrix = 180f;

        updateHealthbarIfPresent();
    }

    private void updateColliderRect() {
        _ColliderRect.set(rectTransform.rect);

        _ColliderRect.left += _SpaceshipModel.colliderOffsetFromTransformRect.left;
        _ColliderRect.right += _SpaceshipModel.colliderOffsetFromTransformRect.right;
        _ColliderRect.top += _SpaceshipModel.colliderOffsetFromTransformRect.top;
        _ColliderRect.bottom += _SpaceshipModel.colliderOffsetFromTransformRect.bottom;
    }

    @Override
    public void update() {
        super.update();

        if (_SpaceshipModel.isShooting)
        {
            long currentMS = System.currentTimeMillis();
            if (currentMS - _TimeLastShoot > _SpaceshipModel.autoShootingDelayMillis)
            {
                shoot();
                _TimeLastShoot = currentMS;
            }
        }
        updateHealthbarIfPresent();
    }

    @Override
    public void draw(Canvas canvas) {
        // This draws the collider (for debugging purposes)
//        Paint p = new Paint();
//        p.setColorFilter(new LightingColorFilter(0, Color.MAGENTA));
//        canvas.drawRect(rectTransform.rect, p);
//
//        Paint p2 = new Paint();
//        p2.setColorFilter(new LightingColorFilter(0, Color.GREEN));
//        canvas.drawRect(_ColliderRect, p2);

        updateDrawMatrix();
        canvas.drawBitmap(_Animation.getCurrentFrameImage(), _DrawMatrix, null);
    }

    @Override
    public int getSelfCollisionLayer() {
        return _SpaceshipModel.selfCollisionLayer;
    }

    @Override
    public int getOthersCollisionLayerMask() {
        return _SpaceshipModel.targetsCollisionLayerMask;
    }

    @Override
    public Rect getCollisionRect() {
        updateColliderRect();
        return _ColliderRect;
    }

    @Override
    public void onCollidedWith(GameObject gameObject, Rect overlapArea) {
        // If collided with a ship, take damage; the other ship will handle its damage intake too
        if (gameObject instanceof Spaceship)
        {
            this.applyDamage(getSpaceshipModel().selfDamageOnCollisionWithAnotherSpaceship);
        }
    }

    @Override
    public void onDestroy() {
        if (_HealthBar != null)
            destroy(_HealthBar);

        if (_SpaceshipModel.explosionModel != null)
        {
            Explosion explosion = new Explosion(_SpaceshipModel.explosionModel);
            explosion.rectTransform.rect.set(rectTransform.rect);
            explosion.rectTransform.scale(1.5f); // 1.5 times the size of the spaceship
            explosion.rectTransform.centerTo(rectTransform.rect.centerX(), rectTransform.rect.centerY());
        }

        super.onDestroy();
    }

    public void setIsShooting(boolean isShooting)
    {
        _SpaceshipModel.isShooting = isShooting;
    }

    public float getHealth() { return _SpaceshipModel.health; }

    public SpaceshipModel getSpaceshipModel() { return _SpaceshipModel; }

    private void updateDrawMatrix()
    {
        _DrawMatrix.reset();
        _DrawMatrix.postTranslate(-_HalfBitmapSize.x, -_HalfBitmapSize.y); // center it to (0,0)
        _DrawMatrix.postScale(_XScaleFactorFromBitmapToRect, _YScaleFactorFromBitmapToRect);
        _DrawMatrix.postRotate(_RotationToSetToDrawMatrix);
        _DrawMatrix.postTranslate(rectTransform.rect.centerX(), rectTransform.rect.centerY());
    }

    void updateHealthbarIfPresent()
    {
        if (_HealthBar != null)
        {
            updateHealthbarPosition();
            updateHealthbarFillAmount();
        }
    }

    // Put the healthbar behind the spaceship
    private void updateHealthbarPosition()
    {
        int yPos = rectTransform.rect.centerY();
        int spaceshipHalfHeight = rectTransform.rect.height() / 2;
        if (_SpaceshipModel.verticalOrientation == SpaceshipModel.VerticalOrientation.DOWN)
            yPos -= spaceshipHalfHeight;
        else
            yPos += spaceshipHalfHeight;

        _HealthBar.updatePosition(rectTransform.rect.centerX(), yPos);
    }

    private void updateHealthbarFillAmount()
    {
        _HealthBar.setFillAmount01(_SpaceshipModel.health);
    }

    // Spawn a Projectile GameObject
    private void shoot()
    {
        int projectileCenterY = rectTransform.rect.centerY();
        if (_SpaceshipModel.verticalOrientation == SpaceshipModel.VerticalOrientation.DOWN)
            projectileCenterY += rectTransform.rect.height()/2;
        else
            projectileCenterY -= rectTransform.rect.height()/2;

        new Projectile(_SpaceshipModel.projectileModel)
                .rectTransform.centerTo(rectTransform.rect.centerX(), projectileCenterY);
    }

    // Damage self
    public void applyDamage(float damage)
    {
        _SpaceshipModel.health -= damage;

        if (_SpaceshipModel.health <= 0f) {
            _SpaceshipModel.health = 0f;

            destroy(this);
        }
        else
        {
            updateHealthbarIfPresent();
        }
    }

    // Move <forceInUnits> forward
    public void applyPropellingForce(int forceInUnits)
    {
        applyPropellingForce(0, -forceInUnits, ForceSpace.LOCAL);
    }

    // Move (x,y) units in world space
    public void applyPropellingForce(int x, int y)
    {
        applyPropellingForce(x, y, ForceSpace.GLOBAL);
    }

    // Move (x,y) units in <forceSpace>
    public void applyPropellingForce(int x, int y, ForceSpace forceSpace)
    {
        if (forceSpace == ForceSpace.LOCAL)
        {
            if (_SpaceshipModel.verticalOrientation == SpaceshipModel.VerticalOrientation.DOWN)
            {
                x = -x;
                y = -y;
            }
        }

        rectTransform.rect.offset(x, y);
        updateHealthbarIfPresent();
    }

    public void silentlyDestroySelf() {
        _SpaceshipModel.explosionModel = null;

        destroy(this);
    }


    // LOCAL: -x = left, +x = right; -y = forward, +y = backwards
    // GLOBAL: -x = world's left, +x = world's right; -y = world's up, +y = world's down
    public enum ForceSpace
    {
        LOCAL,
        GLOBAL
    }
}

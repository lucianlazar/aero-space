package com.ykyriacou.aero_space.controller.shared;

import android.content.Context;
import android.content.SharedPreferences;

import com.ykyriacou.aero_space.controller.GameApplication;
import com.ykyriacou.aero_space.model.constants.GameConfig;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Simple game storage utility class for storing game data. It uses SharedPreferences to store
 * a single json object as a raw String for scores.
 * Stores the sound on/off setting
 *
 *
 * destroy() should be called once the app ends
 */
public class GameStorage {
    private final static String PREF_SCORES_JSON_STRING = "PREF_SCORES_JSON_STRING";
    private final static String PREF_SOUNDS_ON = "PREF_SOUNDS_ON";

    private static GameStorage _Instance;
    private static Context _AppContext;

    private SharedPreferences _Prefs;
    private ArrayList<Integer> _Scores;
    private boolean _SoundsOn;

    private GameStorage()
    {
        _AppContext = GameApplication.getInstance();
        _Prefs = _AppContext.getSharedPreferences("data", Context.MODE_PRIVATE);
        _Scores = new ArrayList<>();
        retrieveScores();
        _SoundsOn = _Prefs.getBoolean(PREF_SOUNDS_ON, true);
    }

    public static GameStorage getInstance()
    {
        if (_Instance == null)
            _Instance = new GameStorage();

        return _Instance;
    }

    public void addNewScore(int score)
    {
        _Scores.add(score);

        // Sort from lowest to biggest
        Collections.sort(_Scores);
        storeScores();
    }

    public ArrayList<Integer> getScores()
    {
        return new ArrayList(_Scores);
    }

    public void setSoundsOn(boolean on)
    {
        _SoundsOn = on;
        _Prefs.edit().putBoolean(PREF_SOUNDS_ON, _SoundsOn).apply();
    }
    public boolean getSoundsOn()
    {
        return _SoundsOn;
    }

    private void storeScores()
    {
        JSONArray jsonArray = new JSONArray(_Scores);
        _Prefs.edit().putString(PREF_SCORES_JSON_STRING, jsonArray.toString()).apply();
    }

    private void retrieveScores()
    {
        _Scores.clear();
        String jsonString = _Prefs.getString(PREF_SCORES_JSON_STRING, "");
        if (jsonString.length() > 0)
        {
            try {
                JSONArray jsonArray = new JSONArray(jsonString);
                for (int i = 0; i < jsonArray.length(); ++i)
                {
                    _Scores.add(jsonArray.getInt(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void destroy()
    {
        _Scores.clear();
        _AppContext = null;
        _Instance = null;
    }

    public int getHighestScore() {
        if (_Scores.size() > 0)
        {
            return _Scores.get(_Scores.size() - 1);
        }
        return -1;
    }
}

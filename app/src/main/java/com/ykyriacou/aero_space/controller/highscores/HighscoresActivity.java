package com.ykyriacou.aero_space.controller.highscores;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ykyriacou.aero_space.R;
import com.ykyriacou.aero_space.controller.shared.GameStorage;
/**
 * Shows the best score
 *
 * All "onClick" the callbacks are defined directly in the activity's layout XML file for convenience
 */
public class HighscoresActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscores);

        String textToSet;
        int score = GameStorage.getInstance().getHighestScore();
        if (score == -1)
        {
            textToSet = "Not played yet";
        }
        else
        {
            textToSet = score + "";
        }

        ((TextView)findViewById(R.id.highscoreTextView)).setText(textToSet);
    }

    public void onBackButtonPressed(View view)
    {
        finish();
    }
}

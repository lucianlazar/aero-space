package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core;

import android.graphics.Point;
import android.graphics.Rect;

/**
 * Used both as a config class for defining dimensions of every object in the world and as a
 * utility that provides scaling factors to transform points from world space to screen space and
 * vice-versa (using worldToScreenX, worldToScreenY and screenToWorldX, screenToWorldY).
 *
 * onSizeChanged(int w, int h) must be called by whoever knows the screen size. Usually, by an activity
 */
public class GameWorldMetrics {
    private static      GameWorldMetrics _Instance;

    public final int    WORLD_WIDTH = 720;
    public final int    WORLD_HEIGHT = 1280;
    public final int    PLAYER_SPACESHIP_WIDTH_HEIGHT = 100;
    public final int    PLAYER_SPACESHIP_COLLIDER_WIDTH_HEIGHT = 60;
    public final int    ENEMY_SPACESHIP_WIDTH_HEIGHT = 80;
    public final int    ENEMY_SPACESHIP_COLLIDER_WIDTH_HEIGHT = 64;
    public final int    PLAYGROUND_PADDING_BOTTOM = 32;
    public final Rect   PLAYGROUND_RECT;
    public final Rect   BACKGROUND_RECT;
    public final int    PROJECTILE_WIDTH = 12;
    public final int    PROJECTILE_HEIGHT = 48;

    public final Point  PLAYER_HEALTHBAR_SIZE = new Point(PLAYER_SPACESHIP_WIDTH_HEIGHT, 8);
    public final Point  ENEMY_SMALL_HEALTHBAR_SIZE = new Point(ENEMY_SPACESHIP_WIDTH_HEIGHT, 5);


    // World->Screen and Screen->World transformation factors.
    // On a screen size of (WORLD_WIDTH, WORLD_HEIGHT) these will all be 1
    public float        worldToScreenX, worldToScreenY;
    public float        screenToWorldX, screenToWorldY;

    private boolean     _Initialized = false;


    public static GameWorldMetrics getInstance()
    {
        if (_Instance == null)
            _Instance = new GameWorldMetrics();

        return _Instance;
    }

    private GameWorldMetrics() {
        PLAYGROUND_RECT = new Rect(0,0,WORLD_WIDTH, WORLD_HEIGHT);
        BACKGROUND_RECT = new Rect(PLAYGROUND_RECT);
    }

    public boolean isInitialized() {
        return _Initialized;
    }


    // Called by whoever knows the screen size :)
    public void onSizeChanged(int w, int h) {
        worldToScreenX = (float)w / WORLD_WIDTH;
        worldToScreenY = (float)h / WORLD_HEIGHT;

        screenToWorldX = 1f / worldToScreenX;
        screenToWorldY = 1f / worldToScreenY;

        _Initialized = true;
    }
}

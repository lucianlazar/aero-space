package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core.GameWorldMetrics;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.SpaceshipAIController;
import com.ykyriacou.aero_space.model.MovingBoundsSettings;
import com.ykyriacou.aero_space.model.SpaceshipModel;

/**
 * Simple enemy spaceship constructed based the model passed in constructor
 *
 * It simply moves forward
 *
 * Can move through GameWorldMetrics.BACKGROUND_RECT
 * Can move outside screen (after which is destroyed)
 */
public class DumbEnemySpaceship extends Spaceship{
    public DumbEnemySpaceship(SpaceshipModel spaceshipModel) {
        super(spaceshipModel);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Spaceship Controller that uses the Joystick to control the player's spaceship
        MovingBoundsSettings movingBoundsSettings = new MovingBoundsSettings();
        movingBoundsSettings.movingArea = GameWorldMetrics.getInstance().BACKGROUND_RECT;
        movingBoundsSettings.outOfBoundsRestriction = MovingBoundsSettings.OutOfBoundsRestriction.ALLOW;

        // The controller is auto-destroyed when the spaceship is destroyed
        new SpaceshipAIController(this, movingBoundsSettings, SpaceshipAIController.MovingBehaviour.DUMB_MOVE_FORWARD);
    }
}

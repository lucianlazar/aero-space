package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.ykyriacou.aero_space.model.SpriteModel;
import com.ykyriacou.aero_space.controller.shared.BitmapCacher;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.BitmapFramesAnimation;

/**
 * Drawable GameObject that can be animated
 */
public class Sprite extends DrawableGameObject {
    protected SpriteModel _SpriteModel;
    protected BitmapFramesAnimation _Animation;
    protected boolean _PlayOnce;

    public Sprite(SpriteModel spriteModel) {
        super(spriteModel.image);
        _SpriteModel = spriteModel;
    }

    @Override
    public void onStart() {
        super.onStart();

        Bitmap[] frames;
        if (_SpriteModel.numAnimationFrames == 1)
            frames = new Bitmap[]{_SpriteModel.image}; // no need to create frames
        else
            frames = BitmapCacher.getInstance().getFrames(_SpriteModel.image, _SpriteModel.numAnimationFrames);

        _Animation = new BitmapFramesAnimation();
        _Animation.setFrames(frames);
    }

    @Override
    public void update() {
        super.update();
        _Animation.update();

        if (_PlayOnce && _Animation.playedOnce())
            destroy(this);
    }

    @Override
    public void draw(Canvas canvas) {
        //super.draw(canvas);
        try{
            canvas.drawBitmap(_Animation.getCurrentFrameImage(), null, rectTransform.rect, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.DrawableGameObject;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.GameObject;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.CollisionDetector;

import java.util.ArrayList;

/**
 * GameObjectsManager handles the lifecycle of all game objects
 *
 * The constructor is public only to allow the activity create a new instance. When the manager
 * is needed from another context, it can be retrieved with getInstance(). It's assumed that
 * no one would ever need it BEFORE the activity creation
 *
 * It dynamically handles object creation (via new GameObjectDerivedClassName()), initialization etc
 *
 * It dispatches update() callbacks each frame to all game objects
 *
 * The phrase "preparing an object" mentioned in below comments means calling an object's onStart()
 * method and registering it as a collider in CollisionDetector if they implement ICollider
 */
public class GameObjectsManager implements GameThread.IOnGameUpdateListener{

    static GameObjectsManager _Instance;

    private ArrayList<GameObject> _AllGameObjects = new ArrayList<>();

    // _DrawableGameObjects is a subset of _AllGameObjects
    private ArrayList<DrawableGameObject> _DrawableGameObjects = new ArrayList<>();

    // "NEW" GameObjects are those added during onGameUpdate or onGamePostUpdate.
    // - In the first case, they are prepared in the same frame and will receive their first update()
    // in the frame after (if they won't be destroyed meanwhile);
    // - In the second case, they are prepared in the next frame and their update() will be called the
    // frame after (if they won't be destroyed meanwhile).
    private ArrayList<GameObject> _NewGameObjects = new ArrayList<>();

    // These are objects destroyed during execution of updateAllGameObjects()
    // They will be removed completely in onGamePostUpdate()
    private ArrayList<GameObject> _GameObjectsToBeRemovedInPostUpdate = new ArrayList<>();

    // Any new object that implements ICollider will be added here also and will receive
    // collision events based on circumstances
    private CollisionDetector _CollisionDetector;

    // Changes to <true> when destroy() is called. It's used as a hint to prevent any more
    // updates or onDestroy callbacks to game objects from being sent.
    // For example, the method updateAllGameObjects() loops through all game objects and sends them
    // update() callbacks. If during one of those callbacks the game needs to be ended
    // (example: player dies), no more game objects shall receive updates and thus, the loop should
    // not continue. The loop in dispatchOnDestroyToDestroyedObjects() acts similarly
    private boolean _IsDestroying;

    public GameObjectsManager() {
        _Instance = this;
        _CollisionDetector = new CollisionDetector();
    }

    // Returns null if GameObjectsManager is being destroyed (or if it's accessed before
    // Activity's onCreate, but it SHALL NOT be the case)
    public static GameObjectsManager getInstance()
    {
        return _Instance;
    }

    /// GameThread callbacks
    @Override
    public void onGameUpdate()
    {
        updateAllGameObjects();
        onGamePostUpdate();
    }

    // Registers game objects
    // Prevents any more object registering if the game is being destroyed
    public boolean addGameObject(GameObject gameObject)
    {
        if (_IsDestroying)
            return false;

        registerGameObjectInternal(gameObject);
        if (gameObject instanceof DrawableGameObject)
        {
            registerDrawableGameObjectInternal((DrawableGameObject)gameObject);
        }

        return true;
    }

    // Simply schedules objects for removal. They won't be removed in the current frame, as most probably
    // the update loop is currently running and iterating through the list of all objects. Instead,
    // the objects are added in a "to be removed at the end of frame" list.
    // The collision detector acts similar, so we can safely request the removal here, though
    public void scheduleGameObjectForRemoval(GameObject gameObject)
    {
        gameObject.onDestroyScheduled();
        _GameObjectsToBeRemovedInPostUpdate.add(gameObject);

        if (gameObject instanceof CollisionDetector.ICollider)
            _CollisionDetector.removeCollider((CollisionDetector.ICollider)gameObject);
    }

    public ArrayList<DrawableGameObject> getDrawableGameObjectsArrayRef() {return _DrawableGameObjects;}

    // This marks the end of the game.
    // Releasing the static instance and game objects.
    // Not passing onDestroyScheduled() and onDestroy() callbacks to game objects, as this may lead
    // to further creation of other ones.
    public void destroy() {
        _IsDestroying = true;
        _Instance = null;

        // Decided not to dispatch the destroy event, as this may lead to mode complicated stuff
        // (they can, for instance, further create/destroy other objects and would interfere in a
        // more complex way with the GameObject lifecycle management)
//        for (int i = 0; i < _AllGameObjects.size(); ++i)
//        {
//            // Already destroyed => skip it
//            if (_AllGameObjects.get(i).isDestroyScheduled())
//                continue;
//
//            try {
//
//                GameObject.destroy(_AllGameObjects.get(i));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }

        _AllGameObjects.clear();
        _NewGameObjects.clear();
        _DrawableGameObjects.clear();
        _GameObjectsToBeRemovedInPostUpdate.clear();
    }


    // Dispatching update to all game objects that are not marked as "to be removed at the end of frame"
    // Notice ".size()" is used in determining the loop's end condition, instead of an int stored
    // before entering it. The reason is _GameObjectsToBeRemovedInPostUpdate may grow during this loop,
    // and the newly added objects must be initialized in the same frame (see prepareObjectToEnterGameWorld(GameObject))
    private void updateAllGameObjects()
    {
        GameObject gameObject;
        for (int i = 0; i < _AllGameObjects.size(); ++i)
        {
            // Stop everything, if the game is ending
            if (_IsDestroying)
                return;

            gameObject = _AllGameObjects.get(i);

            // Skip GameObjects that are scheduled to be removed
            if (_GameObjectsToBeRemovedInPostUpdate.contains(gameObject))
                continue;

            // If the gameObject was added in this frame, tell it to do any last initialization,
            // because it'll receive its first update callback in the next frame.
            // We're not calling its update() immediately in order to make the lifecycle of the game objects
            // more easy to manage. All new game objects created in this frame will receive
            // a callback to their onStart() method, and ONLY in the next frame they'll receive their
            // first update() callback
            if (_NewGameObjects.contains(gameObject))
            {
                prepareObjectToEnterGameWorld(gameObject);
                continue;
            }

            gameObject.update();
        }
        // All new objects were initialized at this point. Clear the array, so it can be used in
        // onGamePostUpdate() too
        _NewGameObjects.clear();
    }

    // Safely removes game objects scheduled for removal, as the update() loop has just ended
    private void onGamePostUpdate()
    {
        dispatchOnDestroyToDestroyedObjects();

        _AllGameObjects.removeAll(_GameObjectsToBeRemovedInPostUpdate);
        _DrawableGameObjects.removeAll(_GameObjectsToBeRemovedInPostUpdate);

        // It can happen. Example: A is destroyed. In A's onDestroy(), B is created,
        // and for some reason B is destroyed in the same frame
        _NewGameObjects.removeAll(_GameObjectsToBeRemovedInPostUpdate);

        // Done. Clear for re-use
        _GameObjectsToBeRemovedInPostUpdate.clear();
    }

    private void prepareObjectToEnterGameWorld(GameObject gameObject)
    {
        gameObject.onStart();
        if (gameObject instanceof CollisionDetector.ICollider)
            _CollisionDetector.addCollider((CollisionDetector.ICollider) gameObject);
    }

    private void registerGameObjectInternal(GameObject gameObject)
    {
        if (_AllGameObjects.contains(gameObject))
            return;

        _AllGameObjects.add(gameObject);

        if (_NewGameObjects.contains(gameObject))
            return;

        _NewGameObjects.add(gameObject);
    }

    private void registerDrawableGameObjectInternal(DrawableGameObject drawableGameObject)
    {
        if (_DrawableGameObjects.contains(drawableGameObject))
            return;
        _DrawableGameObjects.add(drawableGameObject);
    }

    private void dispatchOnDestroyToDestroyedObjects()
    {
        // We're using ".size()" because _GameObjectsToBeRemovedInPostUpdate may grow during this loop.
        // We want to also send an onDestroy() callback to those objects
        // that are scheduled to be destroyed during this loop.
        for (int i = 0; i < _GameObjectsToBeRemovedInPostUpdate.size(); ++i)
        {
            // Stop everything, if the game is ending
            if (_IsDestroying)
                return;

            try {
                _GameObjectsToBeRemovedInPostUpdate.get(i).onDestroy();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

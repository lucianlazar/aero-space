package com.ykyriacou.aero_space.controller.gameplay.subcontrollers;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core.GameWorldMetrics;
import com.ykyriacou.aero_space.R;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors.DumbEnemySpaceship;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors.Spaceship;
import com.ykyriacou.aero_space.model.constants.GameConfig;
import com.ykyriacou.aero_space.model.constants.physics.CollisionLayers;
import com.ykyriacou.aero_space.model.ExplosionModel;
import com.ykyriacou.aero_space.model.ProjectileModel;
import com.ykyriacou.aero_space.model.SpaceshipModel;
import com.ykyriacou.aero_space.model.SpriteModel;
import com.ykyriacou.aero_space.controller.shared.BitmapCacher;

/**
 * Creates enemy spaceships on demand, based on GameConfig.
 */
public class DumbEnemySpaceshipProvider extends SpaceshipProvider {
    // This can be shared, so it's stored here
    final ExplosionModel EXPLOSION_MODEL_FOR_ALL_SPACESHIPS;

    public DumbEnemySpaceshipProvider(Context context) {
        super(context);

        EXPLOSION_MODEL_FOR_ALL_SPACESHIPS = new ExplosionModel(new SpriteModel(R.drawable.sprite_explosion_short, 7));
    }

    @Override
    public Spaceship getNewSpaceship()
    {
// ENEMIES INITIALIZATION
        // Projectile Model
        ProjectileModel projectileModel = new ProjectileModel();
        projectileModel.image = BitmapCacher.getInstance().getForResId(R.drawable.sprite_enemy_projectile);
        projectileModel.movingRect = new Rect(GameWorldMetrics.getInstance().BACKGROUND_RECT);
        projectileModel.selfCollisionLayer = CollisionLayers.ENEMY_PROJECTILE;
        projectileModel.targetsCollisionLayerMask = CollisionLayers.PLAYER;
        projectileModel.explosionDamage = GameConfig.ENEMY_PROJECTILE_DAMAGE;
        projectileModel.size = new Point(GameWorldMetrics.getInstance().PROJECTILE_WIDTH, GameWorldMetrics.getInstance().PROJECTILE_HEIGHT);
        projectileModel.velocity = new Point(0, GameConfig.PROJECTILE_VERTICAL_SPEED); // enemy's projectiles go down
        // it can use the same as the spaceship's one; the size will be adjusted dynamically
        projectileModel.explosionModel = EXPLOSION_MODEL_FOR_ALL_SPACESHIPS;

        // No healthbar for enemies
//        // Healthbar Model
//        HealthBarModel healthBarModel = new HealthBarModel();
//        healthBarModel.image = GameWorld.getInstance().getBitmapCacher().getForResId(R.drawable.sprite_small_health_bar);
//        healthBarModel.colorOnEmpty = GameConfig.HEALTH_BAR_COLOR_ON_EMPTY;
//        healthBarModel.colorOnFull = GameConfig.HEALTH_BAR_COLOR_ON_FULL;
//        healthBarModel.size = GameWorldMetrics.getInstance().ENEMY_SMALL_HEALTHBAR_SIZE;

        // Spaceship Model
        int transformRectSizeToColliderSizeDelta =
                GameWorldMetrics.getInstance().ENEMY_SPACESHIP_WIDTH_HEIGHT
                - GameWorldMetrics.getInstance().ENEMY_SPACESHIP_COLLIDER_WIDTH_HEIGHT;
        SpaceshipModel spaceshipModel = new SpaceshipModel();
        spaceshipModel.spriteModel = new SpriteModel(R.drawable.sprite_enemy, 1);
        spaceshipModel.verticalOrientation = SpaceshipModel.VerticalOrientation.DOWN;
        spaceshipModel.colliderOffsetFromTransformRect =
                new Rect(transformRectSizeToColliderSizeDelta/2,
                        transformRectSizeToColliderSizeDelta/2,
                        -transformRectSizeToColliderSizeDelta/2,
                        -transformRectSizeToColliderSizeDelta/2
                );
        spaceshipModel.autoShootingDelayMillis = GameConfig.AUTO_SHOOTING_DELAY_MILLIS;
        spaceshipModel.maxSpeed = GameConfig.ENEMY_SPACESHIP_MAX_SPEED;
        spaceshipModel.selfDamageOnCollisionWithAnotherSpaceship = GameConfig.ENEMY_SPACESHIP_SELF_DAMAGE_ON_COLLISION_WITH_PLAYER;
        spaceshipModel.selfCollisionLayer = CollisionLayers.ENEMY;
        spaceshipModel.targetsCollisionLayerMask = CollisionLayers.PLAYER;
        spaceshipModel.projectileModel = projectileModel;
//        spaceshipModel.healthBarModel = healthBarModel;
        spaceshipModel.explosionModel = EXPLOSION_MODEL_FOR_ALL_SPACESHIPS;

        // Spaceship GameObject; the SpaceshipController is constructed automatically
        DumbEnemySpaceship spaceship = new DumbEnemySpaceship(spaceshipModel);
        spaceship.rectTransform.setSize(GameWorldMetrics.getInstance().ENEMY_SPACESHIP_WIDTH_HEIGHT);

        return spaceship;
    }
}

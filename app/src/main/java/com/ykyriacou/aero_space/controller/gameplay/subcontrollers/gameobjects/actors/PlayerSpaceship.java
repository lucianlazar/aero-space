package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core.GameWorldMetrics;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.SpaceshipJoystickController;
import com.ykyriacou.aero_space.view.gameplay.Joystick;
import com.ykyriacou.aero_space.model.MovingBoundsSettings;
import com.ykyriacou.aero_space.model.SpaceshipModel;

/**
 * Represents the player's spaceship. Movement is restricted to GameWorldMetrics.PLAYGROUND_RECT
 */
public class PlayerSpaceship extends Spaceship{
    public PlayerSpaceship(SpaceshipModel spaceshipModel) {
        super(spaceshipModel);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Spaceship Controller that uses the Joystick to control the player's spaceship
        MovingBoundsSettings movingBoundsSettings = new MovingBoundsSettings();
        movingBoundsSettings.movingArea = GameWorldMetrics.getInstance().PLAYGROUND_RECT;
        movingBoundsSettings.outOfBoundsRestriction = MovingBoundsSettings.OutOfBoundsRestriction.CLAMP;

        // The controller is auto-destroyed when the spaceship is destroyed
        new SpaceshipJoystickController(this, movingBoundsSettings, Joystick.getInstance());
    }
}

package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors;

import android.graphics.Rect;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.CollisionDetector;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.DrawableGameObject;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.GameObject;
import com.ykyriacou.aero_space.controller.shared.GameAudio;
import com.ykyriacou.aero_space.model.ProjectileModel;

/**
 * A moving projectile that when collides with a target, it damages it and additionally can spawn
 * an explosion
 */
public class Projectile extends DrawableGameObject implements CollisionDetector.ICollider {
    ProjectileModel _ProjectileModel;

    public Projectile(ProjectileModel projectileModel) {
        super(projectileModel.image);
        _ProjectileModel = projectileModel;
        rectTransform.setSize(_ProjectileModel.size.x, _ProjectileModel.size.y);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Play sound effect
        GameAudio.getInstance().playSoundEffect(GameAudio.SFX.SHOOT);
    }

    @Override
    public void update() {
        super.update();

        if (rectTransform.rect.intersect(_ProjectileModel.movingRect))
        {
            // Move with the set velocity
            rectTransform.rect.offset(_ProjectileModel.velocity.x, _ProjectileModel.velocity.y);
        }
        else
        {
            // Projectile outside its assigned move zone => destroy it
            destroy(this);
        }
    }

    @Override
    public int getSelfCollisionLayer() {
        return _ProjectileModel.selfCollisionLayer;
    }

    @Override
    public int getOthersCollisionLayerMask() {
        return _ProjectileModel.targetsCollisionLayerMask;
    }

    @Override
    public Rect getCollisionRect() {
        return rectTransform.rect;
    }

    @Override
    public void onCollidedWith(GameObject gameObject, Rect overlapArea) {
        // Decrease health if collided with target spaceship
        if (gameObject instanceof Spaceship)
        {
            Spaceship spaceship = (Spaceship)gameObject;
            spaceship.applyDamage(_ProjectileModel.explosionDamage);

            // Only create an explosion if the spaceship won't be destroyed
            // (as the spaceship itself will create one, so no need for extra unnecessary computations)
            if (spaceship.getHealth() > 0f)
            {
                if (_ProjectileModel.explosionModel != null)
                {
                    Explosion explosion = new Explosion(_ProjectileModel.explosionModel);
                    explosion.rectTransform.setSize(rectTransform.rect.height());
                    explosion.rectTransform.centerTo(rectTransform.rect.centerX(), rectTransform.rect.centerY());
                }
            }
            else
            {
                if (_ProjectileModel.hitListener != null)
                    _ProjectileModel.hitListener.onSpaceshipDestroyed(spaceship);
            }
        }
        destroy(this);
    }


    public interface IHitListener
    {
        void onSpaceshipDestroyed(Spaceship spaceship);
    }
}

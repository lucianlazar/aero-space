package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * A GameObject that has a visual representation and so needs a draw(Canvas) callback at the end of each frame
 */
public class DrawableGameObject extends GameObject {
    protected Bitmap _Image;

    public DrawableGameObject(Bitmap image)
    {
        super(DrawableGameObject.class.getName());

        _Image = image;
    }

    public void draw(Canvas canvas)
    {
        if (_Image == null)
            return;

        canvas.drawBitmap(_Image, null, rectTransform.rect, null);
    }
}

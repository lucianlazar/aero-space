package com.ykyriacou.aero_space.controller.settings;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.ykyriacou.aero_space.R;
import com.ykyriacou.aero_space.controller.shared.GameAudio;
import com.ykyriacou.aero_space.controller.shared.GameStorage;

/**
 * Simply activates/de-activates the sound setting and stores the preference
 *
 * All "onClick" the callbacks are defined directly in the activity's layout XML file for convenience
 */
public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    public void onSoundOnButtonPressed(View view)
    {
        GameStorage.getInstance().setSoundsOn(true);
        GameAudio.getInstance().resumeMusic();
        Toast.makeText(this,"Sound ON", Toast.LENGTH_SHORT).show();
    }

    public void onSoundOffButtonPressed(View view)
    {
        GameStorage.getInstance().setSoundsOn(false);
        GameAudio.getInstance().pauseMusic();
        Toast.makeText(this,"Sound OFF", Toast.LENGTH_SHORT).show();
    }

    public void onBackButtonPressed(View view)
    {
        finish();
    }
}

package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects;

import android.graphics.Rect;

import java.util.ArrayList;

/**
 * Searches for collisions (overlapping of game objects that implement ICollider) each frame and
 * dispatches the onCollidedWith(GameObject) callback
 *
 *
 *
 *
 */
public class CollisionDetector extends GameObject {

    ArrayList<ICollider> _Colliders = new ArrayList();
    ArrayList<ICollider> _CollidersToBeRemoved = new ArrayList();
    ArrayList<ICollider> _NewColliders = new ArrayList();

    public CollisionDetector()
    {
        super(CollisionDetector.class.getName());
    }

    // Colliders are added to a "pending for adding" list, so they can be added at the start
    // of the next update()
    public void addCollider(ICollider collider)
    {
        //_Colliders.add(collider);
        _NewColliders.add(collider);
    }

    // Colliders are added to a "pending for removal" list, so they can be removed at the start
    // of the next update()
    public void removeCollider(ICollider collider)
    {
        _CollidersToBeRemoved.add(collider);
    }

    @Override
    public void update() {
        super.update();

        // Register new colliders
        if (_NewColliders.size() > 0)
        {
            _Colliders.addAll(_NewColliders);
            _NewColliders.clear();
        }

        // Remove old colliders
        if (_CollidersToBeRemoved.size() > 0)
        {
            _Colliders.removeAll(_CollidersToBeRemoved);
            _CollidersToBeRemoved.clear();
        }

        int size = _Colliders.size();
        ICollider a, b;
        for (int i = 0; i < size; ++i)
        {
            a = _Colliders.get(i);

            // Skip colliders that were scheduled for removal during this frame
            if (_CollidersToBeRemoved.contains(a))
                continue;

            for (int j = i+1; j < size; ++j)
            {
                b = _Colliders.get(j);

                // Skip colliders that were scheduled for removal during this frame
                if (_CollidersToBeRemoved.contains(b))
                    continue;

                boolean sendToA = false, sendToB = false;
                if ((a.getOthersCollisionLayerMask() & b.getSelfCollisionLayer()) != 0)
                    sendToA = true;
                if ((b.getOthersCollisionLayerMask() & a.getSelfCollisionLayer()) != 0)
                    sendToB = true;

                Rect overlapArea = new Rect();
                if (overlapArea.setIntersect(a.getCollisionRect(), b.getCollisionRect()))
                {
                    if (sendToA)
                        a.onCollidedWith((GameObject)b, overlapArea);
                    if (sendToB)
                        b.onCollidedWith((GameObject)a, overlapArea);
                }
            }
        }
    }

    public interface ICollider {
        int getSelfCollisionLayer();
        int getOthersCollisionLayerMask();
        Rect getCollisionRect(); // in global space

        /**
         *
         * @param gameObject
         * @param overlapArea - in world space
         */
        void onCollidedWith(GameObject gameObject, Rect overlapArea);
    }
}

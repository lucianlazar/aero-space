package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.DumbEnemySpaceshipProvider;
import com.ykyriacou.aero_space.R;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors.PlayerSpaceship;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors.Projectile;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors.Spaceship;
import com.ykyriacou.aero_space.model.constants.GameConfig;
import com.ykyriacou.aero_space.model.constants.physics.CollisionLayers;
import com.ykyriacou.aero_space.view.gameplay.Joystick;
import com.ykyriacou.aero_space.model.EnemySpaceshipSpawnerSettings;
import com.ykyriacou.aero_space.model.ExplosionModel;
import com.ykyriacou.aero_space.model.GameplayState;
import com.ykyriacou.aero_space.model.HealthBarModel;
import com.ykyriacou.aero_space.model.ProjectileModel;
import com.ykyriacou.aero_space.model.SpaceshipModel;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors.GameBackground;
import com.ykyriacou.aero_space.model.SpriteModel;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core.GameWorldMetrics;
import com.ykyriacou.aero_space.controller.shared.BitmapCacher;

/**
 * The game's "biggest" GameObject
 *
 * It creates any initial objects (scrolling, background, the player, the enemy spawner etc.)
 * and dispatches relevant callbacks to IGameplayListener (passed in constructor)
 *
 * It maintains, updates and reports the GameplayState to IGameplayListener based on what happens in the world
 * Currently, the game ends when the player dies, so IGameplayListener.onGameOver(GameplayState) is called.
 */
public class GameplayController extends GameObject {
    private Context _Context;
    private GameWorldMetrics _GameWorldMetrics;
    private EnemySpaceshipSpawner _EnemySpaceshipSpawner;
    private PlayerSpaceship _PlayerSpaceship;
    private IGameplayListener _IGameplayListener;
    private GameplayState _GameplayState;

    public GameplayController(Context context, IGameplayListener iGameplayListener) {
        super(GameplayController.class.getSimpleName());

        _Context = context;
        _IGameplayListener = iGameplayListener;
        _GameplayState = new GameplayState();
        _GameWorldMetrics = GameWorldMetrics.getInstance();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ADJUSTING PLAYGROUND BOUNDS BASED ON JOYSTICK's SIZE
        // Joystick's top must be transformed into world's coordinates,
        // since it's a view in the root layout and not a drawable in our GamePanel
        int playgroundBottom = (int)(Joystick.getInstance().getTop() * _GameWorldMetrics.screenToWorldY)
                - _GameWorldMetrics.PLAYGROUND_PADDING_BOTTOM;
        _GameWorldMetrics.PLAYGROUND_RECT.bottom = playgroundBottom;


        // SCROLLING BACKGROUND
        new GameBackground(
                BitmapCacher.getInstance().getForResId(R.drawable.bg_gameplay),
                new Rect(_GameWorldMetrics.BACKGROUND_RECT),
                GameConfig.BACKGROUND_VERTICAL_VELOCITY
        );


        // PLAYER INITIALIZATION
        // Projectile Model
        ProjectileModel projectileModel = new ProjectileModel();
        projectileModel.image = BitmapCacher.getInstance().getForResId(R.drawable.sprite_projectile);
        projectileModel.movingRect = new Rect(_GameWorldMetrics.PLAYGROUND_RECT);
        projectileModel.selfCollisionLayer = CollisionLayers.PLAYER_PROJECTILE;
        projectileModel.targetsCollisionLayerMask = CollisionLayers.ENEMY;
        projectileModel.explosionDamage = GameConfig.PLAYER_PROJECTILE_DAMAGE;
        projectileModel.size = new Point(_GameWorldMetrics.PROJECTILE_WIDTH, _GameWorldMetrics.PROJECTILE_HEIGHT);
        projectileModel.velocity = new Point(0, -GameConfig.PROJECTILE_VERTICAL_SPEED); // player's projectiles go up
        projectileModel.hitListener = new Projectile.IHitListener() {
            @Override
            public void onSpaceshipDestroyed(Spaceship spaceship) {
                ++_GameplayState.spaceshipsDestroyedByPlayersProjectile;
                _IGameplayListener.onPlayerProjectileDestroyedSpaceship(_GameplayState, spaceship);
            }
        };
        projectileModel.explosionModel = new ExplosionModel(new SpriteModel(R.drawable.sprite_explosion_short, 7));

        // Healthbar Model
        HealthBarModel healthBarModel = new HealthBarModel();
        healthBarModel.image = BitmapCacher.getInstance().getForResId(R.drawable.sprite_small_health_bar);
        healthBarModel.colorOnEmpty = GameConfig.HEALTH_BAR_COLOR_ON_EMPTY;
        healthBarModel.colorOnFull = GameConfig.HEALTH_BAR_COLOR_ON_FULL;
        healthBarModel.size = GameWorldMetrics.getInstance().PLAYER_HEALTHBAR_SIZE;

        // Player's Spaceship Model
        int transformRectSizeToColliderSizeDelta =
                GameWorldMetrics.getInstance().PLAYER_SPACESHIP_WIDTH_HEIGHT -
                GameWorldMetrics.getInstance().PLAYER_SPACESHIP_COLLIDER_WIDTH_HEIGHT;

        SpaceshipModel playerSpaceshipModel = new SpaceshipModel();
        playerSpaceshipModel.spriteModel = new SpriteModel(R.drawable.sprite_player, 1);
        playerSpaceshipModel.verticalOrientation = SpaceshipModel.VerticalOrientation.UP;
        playerSpaceshipModel.colliderOffsetFromTransformRect =
                new Rect(transformRectSizeToColliderSizeDelta/2,
                        transformRectSizeToColliderSizeDelta/2,
                        -transformRectSizeToColliderSizeDelta/2,
                        -transformRectSizeToColliderSizeDelta/2
                );
        playerSpaceshipModel.autoShootingDelayMillis = GameConfig.AUTO_SHOOTING_DELAY_MILLIS;
        playerSpaceshipModel.maxSpeed = GameConfig.SPACESHIP_MAX_SPEED;
        playerSpaceshipModel.selfDamageOnCollisionWithAnotherSpaceship = GameConfig.PLAYER_SPACESHIP_SELF_DAMAGE_ON_COLLISION_WITH_ANOTHER_SHIP;
        playerSpaceshipModel.selfCollisionLayer = CollisionLayers.PLAYER;
        playerSpaceshipModel.targetsCollisionLayerMask = CollisionLayers.ENEMY;
        playerSpaceshipModel.projectileModel = projectileModel;
        playerSpaceshipModel.healthBarModel = healthBarModel;
        playerSpaceshipModel.explosionModel = new ExplosionModel(new SpriteModel(R.drawable.sprite_explosion_short, 7));

        // PlayerSpaceship GameObject
        _PlayerSpaceship = new PlayerSpaceship(playerSpaceshipModel);
        // Set initial position and the size from config
        _PlayerSpaceship.rectTransform.setSize(_GameWorldMetrics.PLAYER_SPACESHIP_WIDTH_HEIGHT);
        _PlayerSpaceship.rectTransform.centerToX(_GameWorldMetrics.WORLD_WIDTH / 2);
        _PlayerSpaceship.rectTransform.centerToY(playgroundBottom - _GameWorldMetrics.PLAYER_SPACESHIP_WIDTH_HEIGHT /2);
        _PlayerSpaceship.addOnDestroyListener(new IOnDestroyListener() {
            @Override
            public void onGameObjectDestroyed(GameObject gameObject) {
                // Done
                _IGameplayListener.onGameOver(_GameplayState);
            }
        });

        _GameplayState.playerAlive = true;


        // ENEMIES SPAWNER INITIALIZATION
        EnemySpaceshipSpawnerSettings spawnerSettings = new EnemySpaceshipSpawnerSettings();
        spawnerSettings.spawnIntervalMillis = GameConfig.ENEMY_SPAWNING_INTERVAL_MILLIS;
        // adding +2 so it won't get destroyed immediately after being created for being outside playground
        spawnerSettings.spawnYPosition = (-_GameWorldMetrics.ENEMY_SPACESHIP_WIDTH_HEIGHT / 2)+2;
        spawnerSettings.spawnMinXPosition = _GameWorldMetrics.PLAYER_SPACESHIP_WIDTH_HEIGHT / 2;
        spawnerSettings.spawnMaxXPosition = _GameWorldMetrics.WORLD_WIDTH - _GameWorldMetrics.PLAYER_SPACESHIP_WIDTH_HEIGHT / 2;
        spawnerSettings.spaceshipProvider = new DumbEnemySpaceshipProvider(_Context);

        // The enemy spawner will start spawning spaceships automatically based on settings
        _EnemySpaceshipSpawner = new EnemySpaceshipSpawner(spawnerSettings);

        _IGameplayListener.onGameplayStateInitialized(_GameplayState);
    }

    public interface IGameplayListener
    {
        void onGameplayStateInitialized(GameplayState gameplayState);
        void onPlayerProjectileDestroyedSpaceship(GameplayState gameplayState, Spaceship spaceship);
        void onGameOver(GameplayState gameplayState);
    }
}

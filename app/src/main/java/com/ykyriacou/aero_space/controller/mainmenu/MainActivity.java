package com.ykyriacou.aero_space.controller.mainmenu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.ykyriacou.aero_space.R;
import com.ykyriacou.aero_space.controller.gameplay.GameplayActivity;
import com.ykyriacou.aero_space.controller.highscores.HighscoresActivity;
import com.ykyriacou.aero_space.controller.settings.SettingsActivity;
import com.ykyriacou.aero_space.controller.shared.FontCache;
import com.ykyriacou.aero_space.controller.shared.GameAudio;
import com.ykyriacou.aero_space.controller.shared.GameStorage;

/**
 * App entry activity (main menu)
 *
 * It initializes the GameAudio cache in onCreate
 *
 * It releases several resources in onDestroy(), as there we are sure that the game is exiting
 *
 * All "onClick" the callbacks are defined directly in the activity's layout XML file for convenience
 */
public class MainActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GameAudio.initializeInstance(getApplicationContext());
    }


    @Override
    protected void onDestroy() {

        releaseResources();


        super.onDestroy();
    }

    // Release all resources when the application will exit. MainActivity is the last activity
    private void releaseResources()
    {
        FontCache.getInstance().destroy();
        GameStorage.getInstance().destroy();
        GameAudio.getInstance().destroy();
    }

    public void onAppExitButtonPressed(View view) {
        finish();
    }

    public void onHighscoresButtonPressed(View view) {
        startActivity(new Intent(this, HighscoresActivity.class));
    }

    public void onSettingsButtonPressed(View view) {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    public void onStartButtonPressed(View view) {
        startActivity(new Intent(this, GameplayActivity.class));
    }
}

package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors.Spaceship;
import com.ykyriacou.aero_space.model.MovingBoundsSettings;

/**
 * Base functionality for controlling a spaceship
 */
public class SpaceshipController extends GameObject{
    protected Spaceship _Spaceship;
    protected MovingBoundsSettings _MovingBoundsSettings;

    public SpaceshipController(Spaceship spaceship, MovingBoundsSettings movingBoundsSettings)
    {
        super(SpaceshipController.class.getName());
        _Spaceship = spaceship;
        _MovingBoundsSettings = movingBoundsSettings;
    }

    @Override
    public void update() {
        super.update();

        clampPositionIfNeeded();
    }

    private void clampPositionIfNeeded()
    {
        if (_MovingBoundsSettings.outOfBoundsRestriction == MovingBoundsSettings.OutOfBoundsRestriction.CLAMP)
            _Spaceship.rectTransform.clampToArea(_MovingBoundsSettings.movingArea, false);
    }
}

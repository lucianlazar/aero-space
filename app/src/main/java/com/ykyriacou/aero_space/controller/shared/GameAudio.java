package com.ykyriacou.aero_space.controller.shared;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

import com.ykyriacou.aero_space.R;
import com.ykyriacou.aero_space.model.constants.GameConfig;


/**
 *
 *
 */
public class GameAudio {
    private final int MAX_STREAMS_AT_ONCE = 10;
    private static GameAudio _Instance;

    private SoundPool _SFXSoundPool;
    private MediaPlayer _BGMMediaPlayer;
    private GameStorage _GameStorage;

    private GameAudio(Context appContext)
    {
        _GameStorage = GameStorage.getInstance();

        _BGMMediaPlayer = MediaPlayer.create(appContext,R.raw.bgm_1);
        _BGMMediaPlayer.setLooping(true);

        // Start BGM as soon as it's loaded
        resumeMusic();

        if (GameConfig.GLOBAL_SOUND_EFFECTS_FEATURE_ENABLED)
        {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
                _SFXSoundPool = new SoundPool(MAX_STREAMS_AT_ONCE, AudioManager.STREAM_MUSIC, 0);
            }
            else
            {
                SoundPool.Builder soundPoolBuilder = new SoundPool.Builder();
                soundPoolBuilder.setMaxStreams(MAX_STREAMS_AT_ONCE);
                AudioAttributes.Builder attrsBuilder = new AudioAttributes.Builder();
                attrsBuilder.setContentType(AudioAttributes.CONTENT_TYPE_MUSIC);
                attrsBuilder.setUsage(AudioAttributes.USAGE_MEDIA);
                soundPoolBuilder.setAudioAttributes(attrsBuilder.build());

                _SFXSoundPool = soundPoolBuilder.build();
            }

            // Load all SFXs and store their id;
            for (SFX sfx : SFX.values())
                sfx.pooledSoundId = _SFXSoundPool.load(appContext, sfx.getResId(), 1);
        }
    }

    public static GameAudio getInstance()
    {
        return _Instance;
    }

    public void resumeMusic()
    {
        if (_GameStorage.getSoundsOn())
        {
            if (_BGMMediaPlayer.isPlaying())
                return;
            _BGMMediaPlayer.start();
        }
    }

    public void pauseMusic()
    {
        if (_BGMMediaPlayer.isPlaying())
        {
            _BGMMediaPlayer.pause();
        }
    }

    public void playSoundEffect(SFX sfx)
    {
        if (GameConfig.GLOBAL_SOUND_EFFECTS_FEATURE_ENABLED)
        {
            if (_GameStorage.getSoundsOn())
            {
                _SFXSoundPool.play(sfx.pooledSoundId, .8f, .8f, 1, 0, 1f);
            }
        }
    }

    public void destroy() {
        if (_BGMMediaPlayer != null)
        {
            if (_BGMMediaPlayer.isPlaying())
                _BGMMediaPlayer.stop();

            _BGMMediaPlayer.release();
        }

        if (_SFXSoundPool != null)
            _SFXSoundPool.release();

        _Instance = null;
    }

    public static void initializeInstance(Context appContext) {
        _Instance = new GameAudio(appContext);
    }


    public enum SFX
    {
        EXPLOSION(R.raw.sfx_explosion),
        SHOOT(R.raw.sfx_shoot),
        GAME_OVER(R.raw.sfx_game_over);


        int pooledSoundId;

        private int _ResId;


        SFX(int resId) {
            _ResId = resId;
        }


        int getResId() {return _ResId;}
    }
}

package com.ykyriacou.aero_space.controller.gameplay.subcontrollers;

import android.content.Context;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors.Spaceship;

/**
 * Base class for spaceship providers
 */
public abstract class SpaceshipProvider
{
    protected Context _Context;

    public SpaceshipProvider(Context context)
    {
        _Context = context;
    }

    public abstract Spaceship getNewSpaceship();
}

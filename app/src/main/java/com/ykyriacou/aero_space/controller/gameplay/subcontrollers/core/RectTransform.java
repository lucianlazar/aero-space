package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core;

import android.graphics.Point;
import android.graphics.Rect;

/**
 * A component that every GameObject haves
 *
 * Used to define the object's size/bounds in global space.
 * Point (0,0) represents world's top-left
 * Point (WORLD_WIDTH, WORLD_HEIGHT) represents world's bottom-right
 *
 * The object is drawn inside this rect
 */
public class RectTransform {
    public Rect rect;

    public RectTransform(Rect rect)
    {
        this.rect = new Rect(rect);
    }

    public void centerToX(int x)
    {
        rect.offsetTo(x-Math.round(rect.width()/2f), rect.top);
    }

    public void centerToY(int y)
    {
        rect.offsetTo(rect.left, y-Math.round(rect.height()/2f));
    }

    public void centerTo(int x, int y)
    {
        rect.offsetTo(x - Math.round(rect.width() / 2f), y - Math.round(rect.height() / 2f));
    }

    // Size is set with the top-left corner as pivot (i.e. rect.top and rect.left remain unchanged)
    public void setWidth(int width)
    {
        setSize(width, rect.height());
    }

    // Size is set with the top-left corner as pivot (i.e. rect.top and rect.left remain unchanged)
    public void setHeight(int height)
    {
        setSize(rect.width(), height);
    }

    // Size is set with the top-left corner as pivot (i.e. rect.top and rect.left remain unchanged)
    public void setSize(int widthHeight)
    {
        setSize(widthHeight, widthHeight);
    }

    // Size is set with the top-left corner as pivot (i.e. rect.top and rect.left remain unchanged)
    public void setSize(Point size)
    {
        setSize(size.x, size.y);
    }

    // Size is set with the top-left corner as pivot (i.e. rect.top and rect.left remain unchanged)
    public void setSize(int width, int height)
    {
        rect.set(rect.left, rect.top, rect.left+width, rect.top+height);
    }

    public void clampToArea(Rect area, boolean clipIfCannotFit) {
        if (clipIfCannotFit)
        {
            // TODO if needed
        }
        else
        {
            rect.offsetTo(
                    Math.max(area.left, Math.min(area.right - rect.width(),   rect.left)),
                    Math.max(area.top,  Math.min(area.bottom - rect.height(), rect.top))
            );
        }
    }

    public void scale(float xy) {
        setSize((int)(rect.width() * xy), (int)(rect.height() * xy));
    }
}

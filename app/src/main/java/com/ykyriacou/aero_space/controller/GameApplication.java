package com.ykyriacou.aero_space.controller;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.ykyriacou.aero_space.controller.mainmenu.MainActivity;
import com.ykyriacou.aero_space.controller.shared.GameAudio;

/**
 * The Application class.
 * Used only to pause/resume the background music when the foreground activity is paused/resumed
 */
public class GameApplication extends Application implements Application.ActivityLifecycleCallbacks {

    private static GameApplication _Instance;
    boolean _AtLeastOneActivityResumed;
    boolean _MainActivityAlive; // i.e. not destroyed

    @Override
    public void onCreate() {
        super.onCreate();

        _Instance = this;
        Log.d("GameApplication", "onCreate");

        registerActivityLifecycleCallbacks(this);
    }

    public static GameApplication getInstance() { return _Instance; }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        if (activity.getClass() == MainActivity.class)
            _MainActivityAlive = true;
    }

    @Override
    public void onActivityStarted(Activity activity) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
        _AtLeastOneActivityResumed = true;
        GameAudio.getInstance().resumeMusic();
    }

    @Override
    public void onActivityPaused(Activity activity) {
        _AtLeastOneActivityResumed = false;
        // Pause music only if no activity resumed within 1 second after the activity pause event,
        // which means the whole app paused
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // The app was fully closed meanwhile
                if (!_MainActivityAlive)
                    return;

                if (_AtLeastOneActivityResumed)
                    return;

                GameAudio.getInstance().pauseMusic();
            }
        }, 1000);
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        if (activity.getClass() == MainActivity.class)
            _MainActivityAlive = false;
    }
}

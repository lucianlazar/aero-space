package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors.Spaceship;
import com.ykyriacou.aero_space.model.constants.GameConfig;
import com.ykyriacou.aero_space.view.gameplay.Joystick;
import com.ykyriacou.aero_space.model.MovingBoundsSettings;

/**
 * GameObject that controls a spaceship based on input values from a joystick
 */
public class SpaceshipJoystickController extends SpaceshipController{
    private Joystick _Joystick;
    
    public SpaceshipJoystickController(Spaceship spaceship, MovingBoundsSettings movingBoundsSettings, Joystick joystick)
    {
        super(spaceship, movingBoundsSettings);
        _Joystick = joystick;
    }

    @Override
    public void update() {
        super.update();

        _Spaceship.setIsShooting(_Joystick.getTouched());
        if (_Joystick.getTouched())
        {
            // Only move the spaceship if the input force is big enough (bigger than JOYSTICK_INPUT_ACTIVATION_THRESHOLD)
            float xInput = _Joystick.getHorizontalInput();
            float yInput = _Joystick.getVerticalInput();
            if (Math.abs(xInput) < GameConfig.JOYSTICK_INPUT_ACTIVATION_THRESHOLD)
                xInput = 0f;
            if (Math.abs(yInput) < GameConfig.JOYSTICK_INPUT_ACTIVATION_THRESHOLD)
                yInput = 0f;

            _Spaceship.applyPropellingForce(
                    Math.round(xInput * _Spaceship.getSpaceshipModel().maxSpeed),
                    Math.round(yInput * _Spaceship.getSpaceshipModel().maxSpeed)
            );
        }
    }
}

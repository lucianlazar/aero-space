package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core;

import android.graphics.Canvas;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.ykyriacou.aero_space.model.constants.GameConfig;

/**
 * The game's core thread (not main thread)
 * Provides constant calls to update() and draw(Canvas) and keeps things running
 */
public class GameThread extends Thread {
    private static final long TARGET_DELAY_BETWEEN_FRAMES_MS = Math.round(1000f / GameConfig.FPS_CAP);

    private IOnGameUpdateListener _UpdateListener;
    private SurfaceHolder _SurfaceHolder;
    private Canvas _Canvas;
    private SurfaceView _DrawSurface;
    private boolean _Running;
//    private int _AverageFPS;
    private boolean _Alive;

    public GameThread(IOnGameUpdateListener updateReceiver, SurfaceView drawSurfaceView)
    {
        super();
        _UpdateListener = updateReceiver;
        _DrawSurface = drawSurfaceView;
        _SurfaceHolder = _DrawSurface.getHolder();
        _Alive = true;
    }

    @Override
    public void run()
    {
        long frameStartTimeNano;
        long timeElapsedSinceFrameStartMillis;
        // Targeting a total of TARGET_DELAY_BETWEEN_FRAMES_MS each frame
        long timeRemainingToWaitThisFrameMillis;
//        long timeSinceLastAverageFPSEstimationNano = 0;
//        long frameCountSinceLastAverageFPSEstimation = 0;

        while (_Alive)
        {
            if (_Running)
            {

                frameStartTimeNano = System.nanoTime();
                //_Canvas = null;

                try
                {
                    _Canvas = _SurfaceHolder.lockCanvas();

                    synchronized (_SurfaceHolder)
                    {
                        _UpdateListener.onGameUpdate();
                        _DrawSurface.draw(_Canvas);
                    }
                }
                catch (Exception e) { e.printStackTrace(); }
                finally
                {
                    if (_Canvas != null)
                    {
                        try
                        {
                            _SurfaceHolder.unlockCanvasAndPost(_Canvas);
                        }
                        catch (Exception e) {e.printStackTrace();}
                    }
                }

                timeElapsedSinceFrameStartMillis = Math.round((System.nanoTime() - frameStartTimeNano) / 1000000d);
                timeRemainingToWaitThisFrameMillis = TARGET_DELAY_BETWEEN_FRAMES_MS - timeElapsedSinceFrameStartMillis;

                if (timeRemainingToWaitThisFrameMillis >= 0)
                {
                    try
                    {
                        Thread.sleep(timeRemainingToWaitThisFrameMillis);
                    }
                    catch (Exception e) { e.printStackTrace(); }
                }

//                timeSinceLastAverageFPSEstimationNano += System.nanoTime() - frameStartTimeNano;
//                ++frameCountSinceLastAverageFPSEstimation;

//                if (frameCountSinceLastAverageFPSEstimation == GameConfig.FPS_CAP)
//                {
//                    _AverageFPS = Math.round((1000f/(((float)timeSinceLastAverageFPSEstimationNano/frameCountSinceLastAverageFPSEstimation)/1000000)));
//                    frameCountSinceLastAverageFPSEstimation = timeSinceLastAverageFPSEstimationNano = 0;
//                    //Log.d("TAG", _AverageFPS+"");
//                }
            }
        }
    }

    public void setRunning(boolean running)
    {
        _Running = running;
    }

    public void kill()
    {
        setRunning(false);
        _Alive = false;
    }

    public interface IOnGameUpdateListener
    {
        void onGameUpdate();
    }
}

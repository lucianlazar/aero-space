package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.Sprite;
import com.ykyriacou.aero_space.controller.shared.GameAudio;
import com.ykyriacou.aero_space.model.ExplosionModel;

/**
 * Animated DrawableGameObject that plays one and it's automatically destroyed
 */
public class Explosion extends Sprite {
    public Explosion(ExplosionModel explosionModel) {
        super(explosionModel.spriteModel);

        _PlayOnce = true;
    }

    @Override
    protected void onDidManagerAcceptObject(boolean isAcceptedByManager) {
        super.onDidManagerAcceptObject(isAcceptedByManager);

        if (isAcceptedByManager)
        {
            // Play sound effect immediately after being constructed
            GameAudio.getInstance().playSoundEffect(GameAudio.SFX.EXPLOSION);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }
}

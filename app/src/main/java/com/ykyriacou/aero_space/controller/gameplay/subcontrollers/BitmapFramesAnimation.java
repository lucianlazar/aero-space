package com.ykyriacou.aero_space.controller.gameplay.subcontrollers;

import android.graphics.Bitmap;

/**
 * Used to play an animation.
 *
 * Requires its update() to be called regularly.
 *
 * The draw process is also
 * manually done by retrieving the current frame as a Bitmap with getCurrentFrameImage()
 */
public class BitmapFramesAnimation {
    private Bitmap[] _Frames;
    private int _NumFrames;
    private int _CurrentFrameIndex;
    private long _CurrentFrameStartTimeNano;
    private long _Delay;
    private boolean _PlayedOnce;

    public void setFrames(Bitmap[] frames)
    {
        _Frames = frames;
        _NumFrames = _Frames.length;
        _CurrentFrameIndex = 0;
        _CurrentFrameStartTimeNano = System.nanoTime();
    }

    public void setDelay(long d)
    {
        _Delay = d;
    }

    public void setCurrentFrame(int frameIndex)
    {
        _CurrentFrameIndex = frameIndex;
    }

    public void update()
    {
        long elapsedNano = Math.round((System.nanoTime() - _CurrentFrameStartTimeNano)/1000000d);
        if (elapsedNano > _Delay)
        {
            //_CurrentFrameIndex = (_CurrentFrameIndex+1) % _NumFrames;
            ++_CurrentFrameIndex;
            _CurrentFrameStartTimeNano = System.nanoTime();
        }

        if (_CurrentFrameIndex == _NumFrames)
        {
            _CurrentFrameIndex = 0;
            _PlayedOnce = true;
        }
    }

    public Bitmap getCurrentFrameImage()
    {
        return _Frames[_CurrentFrameIndex];
    }

    public int getCurrentFrameIndex()
    {
        return _CurrentFrameIndex;
    }

    public boolean playedOnce()
    {
        return _PlayedOnce;
    }
}

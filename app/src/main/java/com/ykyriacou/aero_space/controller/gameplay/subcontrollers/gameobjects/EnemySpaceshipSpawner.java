package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors.Spaceship;
import com.ykyriacou.aero_space.model.EnemySpaceshipSpawnerSettings;

import java.util.Random;

/**
 * Randomly spawns spaceships based on settings passed in constructor
 */
public class EnemySpaceshipSpawner extends GameObject {
    EnemySpaceshipSpawnerSettings _EnemySpaceshipSpawnerSettings;
    long _LastTimeSpawnedMillis;
    int _LastSpawnPositionX;
    Random _Random;

    public EnemySpaceshipSpawner(EnemySpaceshipSpawnerSettings enemySpaceshipSpawnerSettings) {
        super(EnemySpaceshipSpawner.class.getName());
        _EnemySpaceshipSpawnerSettings = enemySpaceshipSpawnerSettings;
        _Random = new Random(System.currentTimeMillis());
        _LastSpawnPositionX = (_EnemySpaceshipSpawnerSettings.spawnMinXPosition + _EnemySpaceshipSpawnerSettings.spawnMaxXPosition) / 2;
    }


    @Override
    public void update() {
        super.update();

        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - _LastTimeSpawnedMillis > _EnemySpaceshipSpawnerSettings.spawnIntervalMillis)
        {
            spawnNewSpaceship();
            _LastTimeSpawnedMillis = currentTimeMillis;
        }
    }

    private void spawnNewSpaceship()
    {
        Spaceship spaceship = _EnemySpaceshipSpawnerSettings.spaceshipProvider.getNewSpaceship();
        spaceship.rectTransform.centerTo(
                getRandomSpawnXPosition(spaceship.rectTransform.rect.width()),
                _EnemySpaceshipSpawnerSettings.spawnYPosition);
    }

    int getRandomSpawnXPosition(int spaceshipWidth)
    {
        // Imagine the following: [**-------|----**]
        // [ and ] are space bounds
        // * is space not available for respawning (defined by _EnemySpaceshipSpawnerSettings.spawnMinXPosition and _EnemySpaceshipSpawnerSettings.spawnMaxXPosition)
        // - is empty space
        // | is last spawned X position
        // the "left" and "right" words below are referring to the left and right sides of the last spawned position
        int minXLeft  = _EnemySpaceshipSpawnerSettings.spawnMinXPosition, maxXLeft = _LastSpawnPositionX - spaceshipWidth;
        int minXRight = _LastSpawnPositionX + spaceshipWidth, maxXRight = _EnemySpaceshipSpawnerSettings.spawnMaxXPosition;
        int minXFinal, maxXFinal;
        boolean chooseLeft;

        if (minXLeft < maxXLeft)
        {
            // Last spawn position is not in the the left extremity => choose it
            chooseLeft = true;

            // If the last spawned position is was not in one of the extremities
            // then randomly choose in which area to spawn, with a 1/2 probability for each
            // notice that the minXFinal and maxXFinal are already assigned to left side
            // we only change them to the right side if the chance is met.
            if (minXRight < maxXRight)
                chooseLeft = _Random.nextBoolean();
        }
        else
        {
            // Last spawn position was in the left extremity => choose the right side
            chooseLeft = false;
        }

        if (chooseLeft)
        {
            minXFinal = minXLeft;
            maxXFinal = maxXLeft;
        }
        else
        {
            minXFinal = minXRight;
            maxXFinal = maxXRight;
        }

        int numPossiblePositions = maxXFinal - minXFinal;
        if (numPossiblePositions < 1)
            numPossiblePositions = 1;

        // This goes from 0 to <numPossiblePositions-1> ...
        int chosenPositionDisplacementFromMinPosition = _Random.nextInt(numPossiblePositions);

        // ... which added to <spawnMinXPosition>, goes from <spawnMinXPosition> to <spawnMaxXPosition>
        return _LastSpawnPositionX = minXFinal + chosenPositionDisplacementFromMinPosition;
    }
}

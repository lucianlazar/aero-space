package com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects;

import android.graphics.Rect;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors.Spaceship;
import com.ykyriacou.aero_space.model.MovingBoundsSettings;

/**
 * Simple AI Controller that makes a spaceship behave based on MovingBoundsSettings and MovingBehaviour
 * passed in constructor
 */
public class SpaceshipAIController extends SpaceshipController{

    MovingBehaviour _MovingBehaviour;

    public SpaceshipAIController(Spaceship spaceship, MovingBoundsSettings movingBoundsSettings, MovingBehaviour movingBehaviour)
    {
        super(spaceship, movingBoundsSettings);
        _MovingBehaviour = movingBehaviour;
    }

    @Override
    public void onStart() {
        super.onStart();
		
        //_Spaceship.setIsShooting(true);
	}
	
	
    @Override
    public void update() {
        super.update();

        // Destroy this controller when the spaceship is to be destroyed
        if (_Spaceship.isDestroyScheduled())
        {
            destroy(this);
            return;
        }

        if (_MovingBoundsSettings.outOfBoundsRestriction == MovingBoundsSettings.OutOfBoundsRestriction.ALLOW)
        {
            // Ship outside => destroy it
            if (!Rect.intersects(_Spaceship.rectTransform.rect, _MovingBoundsSettings.movingArea))
            {
                _Spaceship.silentlyDestroySelf();
                return;
            }
        }

        moveSpaceship();
    }

    private void moveSpaceship()
    {
        switch (_MovingBehaviour)
        {
            case DUMB_MOVE_FORWARD:
                _Spaceship.applyPropellingForce(_Spaceship.getSpaceshipModel().maxSpeed);
        }
    }

    public enum MovingBehaviour
    {
        DUMB_MOVE_FORWARD
    }
}

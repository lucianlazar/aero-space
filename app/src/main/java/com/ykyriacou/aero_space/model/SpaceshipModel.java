package com.ykyriacou.aero_space.model;

import android.graphics.Rect;


public class SpaceshipModel {
    public SpriteModel spriteModel;
    public VerticalOrientation verticalOrientation;

    // In game units. All 0 mean the collision rect is the same as the draw rect
    public Rect colliderOffsetFromTransformRect;

    public float health = 1f; // 1f = full
    public boolean isShooting;
    public int autoShootingDelayMillis;
    public int maxSpeed; // in game units
    public float selfDamageOnCollisionWithAnotherSpaceship;

    public int selfCollisionLayer,          // from CollisionLayers
               targetsCollisionLayerMask;   // from CollisionLayers. Can be either a single layer or layer mask

    public ProjectileModel projectileModel; // can be null, if you're sure it'll not be used
    public HealthBarModel healthBarModel; // can be null
    public ExplosionModel explosionModel; // can be null

    public enum VerticalOrientation
    {
        UP,
        DOWN
    }
}

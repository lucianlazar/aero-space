package com.ykyriacou.aero_space.model;

import android.graphics.Bitmap;

import com.ykyriacou.aero_space.controller.shared.BitmapCacher;

/**
 * How should a sprite look?
 * If it has no animation, numAnimationFrames must be set to 1. It basically means it has a one-frame
 * animation. The bitmap is given as a resID for convenience
 */
public class SpriteModel {
    public Bitmap image;
    public int numAnimationFrames;

    public SpriteModel(int bitmapResId, int numAnimationFrames)
    {
        image = BitmapCacher.getInstance().getForResId(bitmapResId);
        this.numAnimationFrames = numAnimationFrames;
    }
}

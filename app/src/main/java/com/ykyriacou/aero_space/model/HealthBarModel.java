package com.ykyriacou.aero_space.model;

import android.graphics.Bitmap;
import android.graphics.Point;

/**
 *
 */
public class HealthBarModel {
    public Bitmap image;
    public Point size; // in game units
    public int colorOnEmpty, colorOnFull;
}

package com.ykyriacou.aero_space.model;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.actors.Projectile;

/**
 *
 */
public class ProjectileModel {
    public Bitmap image;
    public Rect movingRect;                     // in game units
    public int selfCollisionLayer,              // from CollisionLayers
               targetsCollisionLayerMask;       // from CollisionLayers. Can be either a single layer or layer mask
    public float explosionDamage;
    public Point size;                          // in game units
    public Point velocity;                      // in game units per frame
    public Projectile.IHitListener hitListener; // can be null
    public ExplosionModel explosionModel;       // can be null
}

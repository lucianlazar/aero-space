package com.ykyriacou.aero_space.model;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.SpaceshipProvider;

/**
 * Created by NoBody on 09-04-16.
 */
public class EnemySpaceshipSpawnerSettings {
    public SpaceshipProvider spaceshipProvider;
    public int spawnMinXPosition, spawnMaxXPosition; // in game units
    public int spawnYPosition;                       // constant Y position where the spaceships are spawn (in game units)
    public int spawnIntervalMillis;
}

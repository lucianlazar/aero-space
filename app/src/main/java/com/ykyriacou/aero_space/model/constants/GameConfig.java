package com.ykyriacou.aero_space.model.constants;

import android.graphics.Color;


public class GameConfig {
    public static final int FPS_CAP = 30;

    public static final int BACKGROUND_VERTICAL_VELOCITY = 1;

    // Only process input if the current input force MAGNITUDE is greater than this
    // (i.e. less than -JOYSTICK_INPUT_ACTIVATION_THRESHOLD or greater than JOYSTICK_INPUT_ACTIVATION_THRESHOLD)
    public static final float JOYSTICK_INPUT_ACTIVATION_THRESHOLD = .1f;

    public static final int   PROJECTILE_VERTICAL_SPEED = 30;
    public static final float PLAYER_PROJECTILE_DAMAGE = 1f;
    public static final float ENEMY_PROJECTILE_DAMAGE = .2f;

    public static final int AUTO_SHOOTING_DELAY_MILLIS = 300;

    public static final int SPACESHIP_MAX_SPEED = 16;
    public static final int ENEMY_SPACESHIP_MAX_SPEED = 6;

    public static final int ENEMY_SPAWNING_INTERVAL_MILLIS = 2000;

    public static final float PLAYER_SPACESHIP_SELF_DAMAGE_ON_COLLISION_WITH_ANOTHER_SHIP = .34f; // so after 3 collisions is over

    public static final float ENEMY_SPACESHIP_SELF_DAMAGE_ON_COLLISION_WITH_PLAYER = 1f;

    public static final int HEALTH_BAR_COLOR_ON_EMPTY = Color.rgb(200, 0, 0); // relaxing red
    public static final int HEALTH_BAR_COLOR_ON_FULL  = Color.rgb(100, 160, 0); // relaxing green

    public static final boolean GLOBAL_SOUND_EFFECTS_FEATURE_ENABLED = false;
}

package com.ykyriacou.aero_space.model.constants.physics;

/**
 * Used for collision detection in CollisionDetector. Can be used as they are
 * or compounded (bitwise OR'ed, AND'ed etc.) as layer masks
 */
public class CollisionLayers {
    public static final int PLAYER = 1 << 0;
    public static final int ENEMY = 1 << 1;
    public static final int PLAYER_PROJECTILE = 1 << 2;
    public static final int ENEMY_PROJECTILE = 1 << 3;
}

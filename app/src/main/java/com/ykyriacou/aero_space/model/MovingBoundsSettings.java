package com.ykyriacou.aero_space.model;

import android.graphics.Rect;

/**
 *
 */
public class MovingBoundsSettings {
    public Rect movingArea;
    public OutOfBoundsRestriction outOfBoundsRestriction = OutOfBoundsRestriction.CLAMP;

    public enum OutOfBoundsRestriction
    {
        CLAMP, // don't allow tha object to go outside moving area
        ALLOW  // no restrictions
    }
}

package com.ykyriacou.aero_space.view.gameplay;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.core.GameWorldMetrics;
import com.ykyriacou.aero_space.controller.gameplay.subcontrollers.gameobjects.DrawableGameObject;

import java.util.ArrayList;

/**
 * The Android SurfaceView on top of which all the game's graphics will be drawn.
 * One instance per application. It's declared in activity's layout file and it's retrieved
 * by the usual findViewById(int) method
 *
 * The Activity must create the GameThread and pass the instance of this class to it. The GameThread
 * will provide the surface view with constant calls to draw(Canvas), which this class will further
 * propagate to objects in _DrawableGameObjects array
 *
 * It's Activity's job to call initialize(ArrayList<DrawableGameObject>, IGameSurfaceViewLifecycleListener)
 * in its onCreate, after it retrieved the view
 *
 * The activity can also implement IGameSurfaceViewLifecycleListener in order to know when this
 * surface view is created/destroyed
 */
public class GameSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private ArrayList<DrawableGameObject> _DrawableGameObjects;
    private GameWorldMetrics _GameWorldMetrics;
    private IGameSurfaceViewLifecycleListener _GameSurfaceViewLifecycleListener;
    private boolean _IsSurfaceViewCreated;

    public GameSurfaceView(Context context) {
        super(context);
        initializeInternal();
    }

    public GameSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeInternal();
    }

    public GameSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeInternal();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        if (isInEditMode())
            return;

        if (canvas != null)
        {
            int canvasSave = canvas.save();
            // Scale the canvas so that the drawn world will the same size as the SurfaceView's size on the screen
            canvas.scale(_GameWorldMetrics.worldToScreenX,_GameWorldMetrics.worldToScreenY);
            dispatchDrawEventToDrawableGameObjects(canvas);
            // Restore teh canvas scale
            canvas.restoreToCount(canvasSave);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        _IsSurfaceViewCreated = true;
        if (_GameSurfaceViewLifecycleListener != null)
            _GameSurfaceViewLifecycleListener.onGameSurfaceViewCreated();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        _IsSurfaceViewCreated = false;
        if (_GameSurfaceViewLifecycleListener != null)
            _GameSurfaceViewLifecycleListener.onGameSurfaceViewDestroyed();
    }


    public void initialize(ArrayList<DrawableGameObject> drawableObjectsArray, IGameSurfaceViewLifecycleListener gameSurfaceViewLifecycleListener) {
        _DrawableGameObjects = drawableObjectsArray;
        _GameSurfaceViewLifecycleListener = gameSurfaceViewLifecycleListener;
    }

    public boolean isSurfaceViewCreated() {return _IsSurfaceViewCreated;}

    private void initializeInternal() {
        _GameWorldMetrics = GameWorldMetrics.getInstance();
        getHolder().addCallback(this);
    }

    private void dispatchDrawEventToDrawableGameObjects(Canvas canvas)
    {
        DrawableGameObject drawableGameObject;
        for (int i = 0; i < _DrawableGameObjects.size(); ++i)
        {
            drawableGameObject = _DrawableGameObjects.get(i);
            // Draw objects that are not scheduled to be destroyed
            // (ex: GameObject.destroy(obj) was called this frame on them)
            if (drawableGameObject.isDestroyScheduled())
                continue;

            // Skip objects created in this frame or simply those that are not accepted by
            // GameObjectsManager to enter the world
            if (drawableGameObject.isStarted())
                drawableGameObject.draw(canvas);
        }
    }

    // implement IGameSurfaceViewLifecycleListener in order to know when this
    // SurfaceView is created/destroyed
    public interface IGameSurfaceViewLifecycleListener
    {
        void onGameSurfaceViewCreated();
        void onGameSurfaceViewDestroyed();
    }
}

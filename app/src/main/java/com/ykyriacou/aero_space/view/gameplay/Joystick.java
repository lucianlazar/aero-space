package com.ykyriacou.aero_space.view.gameplay;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceView;

/**
 * Used as a virtual joystick. Acts exactly like a real-life one
 *
 * Declare it in an xml. One per activity is allowed
 *
 * Use Joystick.getInstance() to acquire the instance. This makes sense only if the Activity which
 * uses the layout that contains the joystick is running
 */
public class Joystick extends SurfaceView {
    private static Joystick _Instance;

    private int _Width, _Height;
    private boolean _Touched;
    private float _HorizontalInput, _VerticalInput;

    public Joystick(Context context) {
        super(context);
        init();
    }

    public Joystick(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Joystick(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public static Joystick getInstance()
    {
        return _Instance;
    }

    private void init() {
        _Instance = this;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        _Width = w;
        _Height = h;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = event.getAction();
        switch(action){
            case MotionEvent.ACTION_DOWN:
                _Touched = true;
                break;
            case MotionEvent.ACTION_MOVE:
                _Touched = true;
                break;
            case MotionEvent.ACTION_UP:
                _Touched = false;
                break;
            case MotionEvent.ACTION_CANCEL:
                _Touched = false;
                break;
            case MotionEvent.ACTION_OUTSIDE:
                _Touched = false;
                break;
            default:
        }

        if (_Touched)
        {
            float x = event.getX();
            float y = event.getY();

            float normalizedHorizontal = (2 * x /  _Width) - 1f;
            float normalizedVertical   = (2 * y / _Height) - 1f;

            // The values can exceed [-1f, 1f], so clamp them
            _HorizontalInput = Math.max(-1f, Math.min(1f, normalizedHorizontal));
            _VerticalInput =   Math.max(-1f, Math.min(1f, normalizedVertical));
        }
        else
            _HorizontalInput = _VerticalInput = 0f;

        return true; // processed; don't propagate the touch event to other listeners
    }

    // Finger pressing the joystick
    public boolean getTouched()
    {
        return _Touched;
    }

    // -1 = full left
    //  0 = middle
    // +1 = full right
    public float getHorizontalInput()
    {
        return _HorizontalInput;
    }

    // -1 = full down
    //  0 = middle
    // +1 = full up
    public float getVerticalInput()
    {
        return _VerticalInput;
    }
}
